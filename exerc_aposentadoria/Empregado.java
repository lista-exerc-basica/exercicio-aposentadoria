

public class Empregado
{
    private String nome, cpf, dataNascimento;
    private int idade, tempoTrabalhadoEmAnos;
    
    public Empregado() {
    }
    
    public Empregado(String nome, String cpf, String dataNascimento, int idade, int tempoTrabalhadoEmAnos) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.idade = idade;
        this.tempoTrabalhadoEmAnos = tempoTrabalhadoEmAnos;
    }
    
    public boolean idadeMinimaAposentadoria() {
        return this.idade >= 65? true : false;
    }
    
    public boolean tempoMinimoTrabalhadoApodentadoria() {
        return this.tempoTrabalhadoEmAnos >= 30? true : false;  
    }
    
    public boolean tempoMinimoIdadeTempoMinimoTrabalhadoAposentadoria() {
        return (this.tempoTrabalhadoEmAnos >= 25 && this.idade >= 60)? true : false;   
    }
    
    public String toString() {
         return (idadeMinimaAposentadoria() || tempoMinimoTrabalhadoApodentadoria() || tempoMinimoIdadeTempoMinimoTrabalhadoAposentadoria())? 
         "O empregado tem o direito de entrar com o pedido de aposentadoria" : "O empregado não tem o direito de entrar com o pedido de aposentadoria"; 
    }
    
    public void setNome(String nome) {
        this.nome = nome;   
    }
    
    public String getNome() {
        return this.nome;   
    }
    
    public void setCpf(String cpf) {
        this.cpf  = cpf;
    }
    
    public String getCpf() {
        return this.cpf;   
    }
    
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;   
    }
    
    public String getDataNascimento() {
        return this.dataNascimento;   
    }
    
    public void setIdade(int idade) {
        this.idade = idade;   
    }
    
    public int getIdade() {
        return this.idade;   
    }
    
    public void setTempoTabalhadoEmAnos(int tempoTrabalhadoEmAnos) {
        this.tempoTrabalhadoEmAnos = tempoTrabalhadoEmAnos;   
    }
    
    public int getTempoTrablhadoEmAnos() {
        return this.tempoTrabalhadoEmAnos;   
    }
}
