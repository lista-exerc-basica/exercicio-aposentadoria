import java.util.Scanner;


public class Principal
{
    public static void main (String[] args) {
        
        Scanner le = new Scanner(System.in);
        
        Empregado emp = new Empregado();
        System.out.print("Informe o nome do empregado: ");
        emp.setNome(le.next());
        
        System.out.print("Informe o cpf do empregado: ");
        emp.setCpf(le.next());
        
        System.out.print("Informe a data de nascimento do empregado: ");
        emp.setDataNascimento(le.next());
        
        System.out.print("Informe a idade do empregado: ");
        emp.setIdade(le.nextInt());
        
        System.out.print("Informe o tempo trabalho em anos do empregado: ");
        emp.setTempoTabalhadoEmAnos(le.nextInt());
        
        System.out.println(emp.tempoMinimoIdadeTempoMinimoTrabalhadoAposentadoria());
        
        System.out.println(emp.toString());
        
        //Não tem o comportamento esperado no TERMINAL do bluej
        //funciona normal no quando voce instacia direto no bluej
    }
}
